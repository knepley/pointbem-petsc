#!/usr/bin/env python
import os, re, sys

# Figures for SIAM PP Talk:
# ../src/createFigures.py picard.log chord.log newton.log ngmres.log newton_rp_ngmres.log --name=Sphere
# ../src/createFigures.py arg_picard.log arg_newton.log arg_ngmres.log arg_newton_rp_ngmres.log arg_newton_comp_ngmres.log --name=Arginine --basename=solve_arg_slic_
# ../src/createFigures.py arg_picard.log arg_newton.log arg_newton_rp_ngmres.log --name=Arginine --basename=solve_arg_slic_

# Parse log file
def parse(filename, conv = {}, linconv = {}):
  runName  = re.compile('^Running #(\w+):')
  #  7 SNES Function norm 3.519547874704e+00
  snesRes  = re.compile('\s*([0-9]+) SNES Function norm (\d\.\d+e[+-]\d+)')
  snesConv = re.compile('^Nonlinear solve converged due to (\w+) iterations (\d+)')
  snesDiv  = re.compile('^Nonlinear solve did not converge due to (\w+) iterations (\d+)')
  kspConv  = re.compile('\s*Linear solve converged due to (\w+) iterations (\d+)')
  kspDiv   = re.compile('\s*Linear solve did not converge due to (\w+) iterations (\d+)')
  with open(filename) as f:
    for line in f.readlines():
      mn  = runName.match(line)
      mr  = snesRes.match(line)
      mc  = snesConv.match(line)
      md  = snesDiv.match(line)
      mkc = kspConv.match(line)
      mkd = kspDiv.match(line)
      if mn:
        name = mn.group(1)
        conv[name] = []
        linconv[name] = []
        print('Test %s' % name)
      elif mr:
        conv[name].append(mr.group(2))
        if not len(conv[name]) == int(mr.group(1))+1:
          raise RuntimeError('Invalid parse at line:\n%s %d != %d' % (line, len(conv[name]), int(mr.group(1))))
      elif mc:
        print('  Converged in %s iterations' % mc.group(2))
      elif md:
        print('  Diverged in %s iterations' % md.group(2))
      elif mkc:
        linconv[name].append(int(mkc.group(2)))
      elif mkd:
        linconv[name].append(int(mkd.group(2)))
      else:
        pass
        #print(line.strip('\n'))
  return conv, linconv

def makeName(solveDesc, linearIts):
  solver = solveDesc[:-2]
  if   solver == 'newton': name = 'Newton'
  elif solver == 'chord':  name = 'Chord'
  elif solver == 'picard': name = 'Picard'
  elif solver == 'nrich':  name = 'Richardson'
  elif solver == 'ngmres': name = 'Anderson'
  elif solver == 'newton_rp_ngmres':   name = 'Newton$-_{R}$Anderson'
  elif solver == 'newton_comp_ngmres': name = 'Newton$+$Anderson'
  if linearIts:
    name += ' (%d)' % linearIts
  return name

def convergencePlot(conv, linconv, problem, basename, useLinear = None):
  import matplotlib.pyplot as plt

  color   = {'nrich': 'r', 'ngmres': 'g', 'newton': 'b', 'chord': 'k', 'picard': 'r', 'newton_rp_ngmres': 'm', 'newton_comp_ngmres': 'y'}
  marker  = {'0': '+', '1': 'x', '5': 'x', '7': '-'}
  data    = []
  legends = []
  for run in sorted(conv.keys()):
    desc = run[len(basename):]
    meshNum = desc[-1]
    if useLinear:
      its = [0]+[sum(linconv[run][:i+1]) for i in range(len(linconv[run]))]
    else:
      its = range(0, len(conv[run]))
    data.extend([its, conv[run], color[desc[:-2]]+marker[meshNum]])
    legends.append(makeName(desc, sum(linconv[run])))
  SizeError = plt.semilogy(*data)
  plt.title('Nonlinear Convergence of SLIC for '+problem)
  if useLinear:
    plt.xlabel('Iteration Number')
  else:
    plt.xlabel('Linear Iteration Number')
  plt.ylabel(r'Residual $\|\|F(\sigma)\|\|_2$')
  plt.legend(legends)
  plt.gcf().set_size_inches(16.18,10)
  if useLinear:
    plt.savefig('SLICConv'+problem+'LinearIts.pdf')
  else:
    plt.savefig('SLICConv'+problem+'.pdf')
  plt.show()
  return


if __name__ == '__main__':
  # Argument parsing
  import argparse

  parser = argparse.ArgumentParser(description     = 'PETSc BEM Test System',
                                   epilog          = 'For more information, visit https://www.mcs.anl.gov/petsc',
                                   formatter_class = argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--version', action='version', version='PETSc BuildSystem 0.1')
  parser.add_argument('files', nargs='*', help='Log files to parse')
  parser.add_argument('--name', default='Unknown', help='The problem name, e.g. Sphere')
  parser.add_argument('--basename', default='solve_slic_', help='The logfile basename')
  parser.add_argument('--linits', default=None, action='store_const', const=True, help='Use linear iterates, instead of nonlinear, as the x-axis')

  args = parser.parse_args()
  conv    = {}
  linconv = {}
  for fn in args.files:
    parse(fn, conv, linconv)
  convergencePlot(conv, linconv, args.name, args.basename, args.linits)
