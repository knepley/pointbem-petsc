#if !defined(__SURFACE_H)
#define __SURFACE_H

typedef struct _n_PetscSurface *PetscSurface;
struct _n_PetscSurface {
  DM        dmV;           /* DM for vertex storage */
  Vec       vertexAreas;   /* Areas associated with vertices */
  Vec       vertexNormals; /* Normals associated with vertices */
  DM        dmC;           /* DM for cell storage */
  Vec       cellAreas;     /* Cell areas */
  Vec       cellNormals;   /* Cell normals */
  PetscReal totalArea;     /* Total surface area calculated from cell areas */
};

PETSC_EXTERN PetscErrorCode DMPlexCreateBardhanFromFile(MPI_Comm, const char[], PetscBool, Vec *, DM *);
PETSC_EXTERN PetscErrorCode DMPlexCreateBardhan(MPI_Comm, PetscViewer, PetscViewer, PetscBool, Vec *, DM *);

PETSC_EXTERN PetscErrorCode loadSrfIntoSurfacePoints(MPI_Comm, const char[], Vec *, Vec *, Vec *, PetscReal *, DM *);
PETSC_EXTERN PetscErrorCode makeSphereSurface(MPI_Comm, PetscReal[], PetscReal, PetscInt, Vec *, Vec *, Vec *, DM *);

PETSC_EXTERN PetscErrorCode PetscSurfaceCreate(DM, PetscSurface *);
PETSC_EXTERN PetscErrorCode PetscSurfaceCreateMSP(MPI_Comm, const char[], PetscSurface *);
PETSC_EXTERN PetscErrorCode PetscSurfaceDestroy(PetscSurface *);
#endif
