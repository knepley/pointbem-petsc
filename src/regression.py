#!/usr/bin/env python
import os, sys

# Find PETSc/BuildSystem
if 'PETSC_DIR' in os.environ:
  configDir = os.path.join(os.environ['PETSC_DIR'], 'config')
  bsDir     = os.path.join(configDir, 'BuildSystem')
  sys.path.insert(0, bsDir)
  sys.path.insert(0, configDir)

localRegressionParameters = {}

import script

def noCheckCommand(command, status, output, error):
  ''' Do no check result'''
  return

class IdentityParser(object):
  def __init__(self):
    return

  def parse(self, text):
    return text, ''

class Runner(script.Script):
  def __init__(self):
    self.petscDir = os.environ['PETSC_DIR']
    # self.mpiexec = self.configInfo.mpi.mpiexec
    self.mpiexec = 'mpiexec'
    self.dryRun  = False
    self.logFile = 'example.log'

  def logPrint(self, message): self.log.write(message+'\n')

  def logWrite(self, message): self.log.write(message)

  def setup(self):
    self.log = file(self.logFile, 'w')
    self.out = sys.stdout

  def cleanup(self):
    self.log.close()

  def checkTestOutputGeneric(self, parser, testDir, executable, cmd, output, testNum):
    from difflib import unified_diff
    outputName = os.path.join(testDir, 'output', os.path.basename(executable)+'_'+testNum+'.out')
    ret        = 0
    with file(outputName) as f:
      output                  = output.strip()
      parse, excess           = parser.parse(output)
      validOutput             = f.read().strip().replace('\r', '') # Jed is now stripping output it appears
      validParse, validExcess = parser.parse(validOutput)
      if not validParse == parse or not validExcess == excess:
        self.logPrint("TEST ERROR: Regression output for %s (test %s) does not match\n" % (executable, testNum))
        for linum,line in enumerate(unified_diff(validOutput.split('\n'), output.split('\n'), fromfile=outputName, tofile=cmd)):
          end = '' if linum < 3 else '\n' # Control lines have their own end-lines
          self.logWrite(line+end)
        self.logPrint('Reference output from %s\n' % outputName)
        self.logPrint(validOutput)
        self.logPrint('Current output from %s' % cmd)
        self.logPrint(output)
        ret = -1
      else:
        self.logPrint("TEST SUCCESS: Regression output for %s (test %s) matches" % (executable, testNum))
    return ret

  def checkTestOutput(self, numProcs, testDir, executable, cmd, output, testNum):
    return self.checkTestOutputGeneric(IdentityParser(), testDir, executable, cmd, output, testNum)

  def getTestCommand(self, executable, **params):
    numProcs = params.get('numProcs', 1)
    try:
      args   = params.get('args', '') % dict(meshes=os.path.join(self.petscDir,'share','petsc','datafiles','meshes'))
    except ValueError:
      args   = params.get('args', '')
    hosts    = ','.join(['localhost']*int(numProcs))
    return ' '.join([self.mpiexec, '-host', hosts, '-n', str(numProcs), os.path.abspath(executable), args])

  def runTest(self, testDir, executable, testNum, replace, **params):
    '''testNum can be any string'''
    num = str(testNum)
    cmd = self.getTestCommand(executable, **params)
    numProcs = params.get('numProcs', 1)
    self.logPrint('Running #%s: %s' % (num, cmd))
    if not self.dryRun:
      (output, error, status) = self.executeShellCommand(cmd, checkCommand = noCheckCommand, log=self.log)
      outputName = os.path.join(testDir, 'output', os.path.basename(executable)+'_'+num+'.out')
      if status:
        self.logPrint("TEST ERROR: Failed to execute %s\n" % executable)
        self.logPrint(output+error)
        ret = -2
      elif replace:
        outputName = os.path.join(testDir, 'output', os.path.basename(executable)+'_'+str(testNum)+'.out')
        with file(outputName, 'w') as f:
          f.write(output+error)
        self.logPrint("REPLACED: Regression output file %s (test %s) was stored" % (outputName, str(testNum)))
        ret = 0
      elif not os.path.isfile(outputName):
        self.logPrint("MISCONFIGURATION: Regression output file %s (test %s) is missing" % (outputName, testNum))
        ret = 0
      else:
        ret = getattr(self, 'checkTestOutput'+params.get('parser', ''))(numProcs, testDir, executable, cmd, output+error, num)
    return ret

def checkSingleRun(runner, ex, replace, extraArgs = '', isRegression = False, testRe = None):
  import shutil

  if isinstance(ex, list):
    exampleName = os.path.splitext(os.path.basename(ex[0]))[0]
    exampleDir  = os.path.dirname(ex[0])
  else:
    exampleName = os.path.splitext(os.path.basename(ex))[0]
    exampleDir  = os.path.dirname(ex)
  executable  = ex
  paramKey    = os.path.join(os.path.relpath(exampleDir, runner.petscDir), os.path.basename(executable))
  if not exampleDir in localRegressionParameters: return
  params = localRegressionParameters[exampleDir].get(paramKey, {})
  if isRegression and not params: return
  if not isinstance(params, list): params = [params]
  numtests = len(params)
  runner.logPrint('Running %d tests\n' % (numtests,))
  for testnum, param in enumerate(params):
    testnum = str(testnum)
    if 'num' in param:
      testnum = param['num']
      if testRe and not testRe.match(testnum):
        runner.logPrint('Skipping test %s\n' % (testnum))
        continue
    if 'numProcs' in args and not args.numProcs is None:
      param['numProcs'] = args.numProcs
    if not 'args' in param: param['args'] = ''
    param['args'] += extraArgs
    if runner.runTest(exampleDir, executable, testnum, replace, **param):
      print('TEST RUN FAILED (check example.log for details)')
      return 1
  return 0

def check(args):
  '''Check that build is functional'''
  ret       = 0
  extraArgs = ' '+' '.join(args.args)
  testRe    = None
  runner    = Runner()
  runner.setup()
  if 'regParams' in args and not args.regParams is None:
    mod = __import__(args.regParams)
    localRegressionParameters[os.path.dirname(mod.__file__)] = mod.regressionParameters
  if 'tests' in args:
    import re
    testRe = re.compile(args.tests)
  examples = []
  for f in args.files:
    if f[0] == '[':
      examples.append(map(os.path.abspath, f[1:-1].split(',')))
    else:
      examples.append(os.path.abspath(f))
  for ex in examples:
    ret = checkSingleRun(runner, ex, args.replace, extraArgs, testRe = testRe)
    if ret: break
  if not ret:
    print('All tests pass')
  runner.cleanup()
  return ret

if __name__ == '__main__':
  # Argument parsing
  import argparse

  parser = argparse.ArgumentParser(description     = 'PETSc Test System',
                                   epilog          = 'For more information, visit https://www.mcs.anl.gov/petsc',
                                   formatter_class = argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--version', action='version', version='PETSc BuildSystem 0.1')
  subparsers = parser.add_subparsers(help='build actions')

  parser_check = subparsers.add_parser('check', help='Check that build is functional')
  parser_check.add_argument('files', nargs='*', help='Extra examples to test')
  parser_check.add_argument('--args', action='append', default=[], help='Extra execution arguments for test')
  parser_check.add_argument('--replace', action='store_true', default=False, help='Replace stored output with test output')
  parser_check.add_argument('--numProcs', help='The number of processes to use')
  parser_check.add_argument('--regParams', help='A module for regression parameters')
  parser_check.add_argument('--tests', help='A regular expression for test names')
  parser_check.set_defaults(func=check)

  args = parser.parse_args()
  args.func(args)
